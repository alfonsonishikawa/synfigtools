/**
 * This script implements a wrapper for Gettext
 *
 * The main translation method is `_(text)`, but if used with 2 parameters, the first will be the context, so
 * this two are equivalent:
 *
 * ```
 * _( 'context', 'text' )
 * _x( 'context', 'text' )
 * ```
 *
 * The third method allows for plural depending on a quantity:
 *
 * ```
 * _n ( 'singular', 'plural', value )
 * ```
 *
 * This script depends of a former loaded file with the translations (`resources/i18n/xx.js`)
 *
 * @type {null}
 */

/**
 * A former file must have defined the variable `json_locale_data` with Gettext.js format to configure the translator.
 * @type {Object}
 * @private
 */
var json_locale_data = json_locale_data || null ;

// If no file has been loaded, we notify the user and fallback to english
if (json_locale_data === null) {
    var language = localStorage ? (localStorage.getItem('user-language') || 'en') : 'en' ;
    console.error('Locale files for language "' + language + '" not found. Resetting to "en". Please reload the application') ;
    alert('Locale files for language "' + language + '" not found. Resetting to "en". Please reload the application') ;
    localStorage.setItem('user-language', 'en') ;
}

/**
 * The wrapped translation instance
 * @type {Gettext}
 * @private
 */
var gt = new Gettext({
    "domain": "root",
    "locale_data": json_locale_data
});

/**
 * Translates a text when used as `_('text')`.
 * When used with 2 parameters behaves similar to `_x()`: the first is the context and the second the text to translate.
 * @param {String} keyOrCntx - String with the texto to translate, or the context when passing 2 parameters
 * @param {String} [optionalKey] - String with the text to translate withing the given context
 * @public
 */
function _(keyOrCntx, optionalKey) {
    if (optionalKey) {
        return _x(keyOrCntx, optionalKey)
    } else {
        return gt.gettext(keyOrCntx) ;
    }
}

/**
 * Translates a text giving the singular or plural translation depending on the value. Userful for creating texts like: _1 apple_, _2 apples_.
 *
 * @param {String} singular - Singular word to translate
 * @param {String} plural - Plural word to tanslate
 * @param {Number} value - Value to decide if translate in singular o plural
 * @param {String} [context] - Translates within the given context
 * @public
 */
function _n(singular, plural, value, context) {
    if (context) {
        gt.npgettext(context, singular, plural, value) ;
    } else {
        return gt.ngettext(singular, plural, value) ;
    }
}

/**
 * Translates a text withing the given context
 * @param {String} context
 * @param {String} key
 * @public
 */
function _x(context, key){
    return gt.pgettext(context, key) ;
}