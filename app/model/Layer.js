Ext.define('SynfigTools.model.Layer', {
    extend: 'Ext.data.TreeModel',

    fields: [
        { name: 'name', type: 'string' },
        { name: 'type', type: 'string' },
        { name: 'isReference', type: 'boolean' },
        { name: 'referencedFile', type: 'string' },
        /**
         * The XML Node with the full definition of the layer.
         */
        { name: 'xmlNode', type: 'auto' }
    ],

    /**
     * Given a XmlElement, fill this model data
     * @param {XmlElement} xmlElement
     */
    loadFromXmlElement: function(xmlElement) {
        var me = this,
            name = xmlElement.getAttribute('desc'),
            type = xmlElement.getAttribute('type'),
            paramCanvasUseElement = xmlElement.querySelector(':scope > param[name=canvas][use]') ;

        if (type === 'PasteCanvas') {
            type = 'group' ;
        }
        if (type === 'zoom' && Ext.isEmpty(name)) {
            name = 'Scale' ;
        }
        if (Ext.isEmpty(name)) {
            name = Ext.String.capitalize(type) ;
        }

        me.set('name', name) ;
        me.set('type', type) ;
        me.set('isReference', false) ;
        me.set('leaf', true) ;
        me.set('xmlNode', xmlElement) ;
        me.set('iconCls', 'synfig-' + type + '-icon') ;
        switch (type) {
            case 'group' :
            case 'switch':
                me.set('leaf', false) ;
                break ;
            case 'import':
                me.set('referencedFile', xmlElement.querySelector(':scope > param[name=filename] > string').textContent) ;
                break ;
            case 'skeleton':
                me.set('name', xmlElement.querySelector(':scope > param[name=name] > string').textContext) ;
                break ;
            default:

        }
        if (paramCanvasUseElement) {
            me.set('leaf', true) ;
            me.set('isReference', true) ;
            me.set('referencedFile', paramCanvasUseElement.getAttribute('use')) ;
        }

    }

}) ;