Ext.define('SynfigTools.view.DesktopApp', {
    extend: 'Ext.ux.desktop.App',

    requires: [
        'SynfigTools.view.modules.AboutModule',
        'SynfigTools.view.modules.ImportCopyModule'
    ],

    uses: [
        'SynfigTools.view.modules.ImportCopyModule',
        'Ext.menu.Separator',
        'SynfigTools.view.modules.AboutModule',
        'SynfigTools.view.modules.ModsModule',
        'SynfigTools.view.modules.SettingsModule',
        'SynfigTools.view.modules.TestsModule'
    ],

    desktopConfig : {
        cls: 'ux-wallpaper',
        //contextMenuItems: [],
        shortcuts: Ext.create('Ext.data.Store', {
            model: 'Ext.ux.desktop.ShortcutModel',
            data: [
                {
                    name: _('Import (copy)'),
                    shownName: _('Copy&nbsp;layers'),
                    iconCls: 'icon-import',
                    module: 'importcopymodule'
                },
                {
                    name: _('Mods'),
                    shownName: _('Mods'),
                    iconCls: 'icon-puzzle',
                    module: 'modsmodule'
                }
            ]
        }),

        shortcutTpl: [
            '<tpl for=".">',
            '<div class="ux-desktop-shortcut" id="{name}-shortcut">',
            '<div class="ux-desktop-shortcut-icon {iconCls}">',
            '<img src="',Ext.BLANK_IMAGE_URL,'" title="{name}">',
            '</div>',
            '<span class="ux-desktop-shortcut-text">{shownName}</span>',
            '</div>',
            '</tpl>',
            '<div class="x-clear"></div>'
        ],
    },

    taskbarConfig: {
        startBtnText: _('Tools'),
        trayItems: [
            {
                xtype: 'trayclock'
            }
        ]
    },

    startConfig: {
        title: _('Synfig Tools'),
        minHeight: 300,
        minWidth: 430,
        toolConfig: {
            width: 190,
            items: [
                {
                    text: _('Settings'),
                    iconCls: 'x-fa fa-gears',
                    handler: function() {
                        var me = this.up('desktop').app ;
                        me.createWindow(me.getModule('settingsmodule')) ;
                    }
                }//,
                // '-',
                // {
                //     text: _('Development tests'),
                //     iconCls: 'x-fa fa-times-circle',
                //     handler: function() {
                //         var me = this.up('desktop').app ;
                //         me.createWindow(me.getModule('testsmodule')) ;
                //     }
                // }
            ]
        }
    },

    /**
     * Must define the modules in a method, not statically
     * @return [Ext.ux.desktop.Module[]] modules instances.
     */
    getModules: function() {
        return [
            Ext.create('SynfigTools.view.modules.ImportCopyModule'),
            Ext.create('SynfigTools.view.modules.ModsModule'),
            Ext.create('Ext.menu.Separator',{launcher: { handler: function() {} }}),
            Ext.create('SynfigTools.view.modules.AboutModule'),
            Ext.create('SynfigTools.view.modules.SettingsModule'),
            Ext.create('SynfigTools.view.modules.TestsModule')
        ] ;
    }

}) ;