Ext.define('SynfigTools.view.settings.LanguageController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.language',

    /**
     * The init function is called after initComponent, before render.
     * At this moment, the language option at the localStorage must be configured to be shown in the split button.
     */
    init: function() {
        var language = localStorage ? (localStorage.getItem('user-language') || 'en') : 'en',
            buttonView = this.getView(),
            buttonText = 'English';

        buttonView.setIconCls(language);
        var currentLanguageMenuItem = buttonView.getMenu().items.findBy(
                function ( item, key ) {
                    return item.iconCls === language ;
                }
            ) ;

        if (currentLanguageMenuItem !== null) {
            buttonText = currentLanguageMenuItem.text ;
        }
        buttonView.setText(buttonText) ;
    },

    /**
     * Handles the selection of a language in the Language split button. Shows the selected language.
     * Sets in the local Storage the 'user-language' key with the iconCls selected
     * @param item - Selected menu option (language)
     * @param e
     * @param options
     */
    onMenuItemClick: function(item, e, options){
        var menu = this.getView() ;
        menu.setIconCls(item.iconCls) ;
        menu.setText(item.text) ;
        localStorage.setItem("user-language", item.iconCls) ;
        Ext.MessageBox.confirm(
            _('Reload the application?'),
            _('The application must be reloaded to load the selected language. Restart?'),
            function(answer) {
                if (answer === 'yes') {
                    window.location.reload() ;
                }
            }
        ) ;
    }

}) ;