Ext.define('SynfigTools.view.settings.Settings', {
    extend: 'Ext.window.Window',

    title: _('Settings'),

    requires: [
        'SynfigTools.view.settings.Language',
        'Ext.form.FieldContainer'
    ],

    iconCls: 'x-fa fa-gears',
    bodyCls: 'window-body-padding',

    width: 600,
    height: 400,
    minWidth: 450,
    minHeight: 300,
    scrollable: true,

    layout: 'vbox',

    items: [
        {
            xtype: 'fieldcontainer',
            layout: 'hbox',
            fieldLabel: _('Language'),
            labelWidth: 90,
            items: [
                {
                    xtype: 'language',
                    name: 'language'
                }
            ]
        }
    ],
    buttons: [
        {
            text: _('Close'),
            handler: function() {
                this.up('window').close() ;
            }
        }
    ],

}) ;