Ext.define('SynfigTools.view.settings.Language', {
    extend: 'Ext.button.Split',
    alias: 'widget.language',

    requires: [
        'SynfigTools.view.settings.LanguageController'
    ],

    controller: 'language',

    width: 130,

    menu: {
        xtype: 'menu',
        defaults: {
            listeners: {
                click: 'onMenuItemClick'
            }
        },
        items: [
            {
                xtype: 'menuitem',
                iconCls: 'en',
                text: 'English'
            },
            {
                xtype: 'menuitem',
                iconCls: 'es',
                text: 'Español'
            }
        ]
    }
}) ;