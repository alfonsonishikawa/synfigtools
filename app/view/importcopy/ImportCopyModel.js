Ext.define('SynfigTools.view.importcopy.ImportCopyModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.importcopy',

    requires: [
        'SynfigTools.model.Layer'
    ],

    data: {
        /**
         * Document with the XML tree
         */
        leftFileXmlDocument: null,

        /**
         * Document with the XML tree
         */
        rightFileXmlDocument: null
    },

    stores: {
        rightLayersStore: {
            type: 'tree',
            model: 'SynfigTools.model.Layer',
            rootVisible: false,
            root: {
                expanded: true,
                children: []
            }
        },

        leftLayersStore: {
            type: 'tree',
            model: 'SynfigTools.model.Layer',
            rootVisible: false,
            root: {
                expanded: true,
                children: []
            }
        }
    },

    formulas: {
        leftFileTitle: function(get) {
            var leftFileXmlDocument = get('leftFileXmlDocument') ;

            if (leftFileXmlDocument === null) return '' ;
            return leftFileXmlDocument.querySelector('canvas name').textContent ;
        },

        rightFileTitle: function(get) {
            var rightFileXmlDocument = get('rightFileXmlDocument') ;

            if (rightFileXmlDocument === null) return '' ;
            return rightFileXmlDocument.querySelector('canvas name').textContent ;
        },

        /**
         * Flags when the copy button is disabled:
         *
         * Enabled: when a left file has been loaded && a right layer has been selected.
         * @param get
         * @return {boolean}
         */
        buttonCopyDisabled: function(get) {
            return !(get('leftFileXmlDocument') !== null && get('rightLayersTreePanel.selection') !== null) ;
        },

        /**
         * Flags when the copy button is disabled:
         *
         * Enabled: when a left and right layers have been selected.
         * @param get
         * @return {boolean}
         */
        buttonMergeDisabled: function(get) {
            return !(get('leftLayersTreePanel.selection') !== null && get('rightLayersTreePanel.selection') !== null) ;
        }
    }

}) ;