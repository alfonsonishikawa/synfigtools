Ext.define('SynfigTools.view.importcopy.ImportCopyController', {
    extend: 'Ext.app.ViewController',

    alias:'controller.importcopy',

    uses: [
        'SynfigTools.view.importcopy.ImportCopyUtils',
        'SynfigTools.view.importcopy.MergeOptions'
    ],

    /**
     * Handler for when a source file is selected. It copies the content to the ViewModel.
     * @param {Ext.form.field.File} fileField
     * @param {String} path
     * @param {Object} eOpts
     */
    onLeftFileUpload: function (fileField, path, eOpts) {
        var me = this,
            viewModel = me.getViewModel() ;

        me._loadFile(fileField, 'leftFileXmlDocument', fileField).then(
            function(figXmlDocument) {
                var leftLayersStore = viewModel.get('leftLayersStore') ;
                leftLayersStore.getRoot().removeAll(true) ;
                SynfigTools.importCopyUtils.fillLayersStore(leftLayersStore, figXmlDocument) ;
                leftLayersStore.commitChanges() ;
            }
        ).otherwise(
            function(error) {
                SynfigTools.toastHelper.showErrorToast(error.message) ;
                Ext.log({
                    msg: error.stack,
                    level: 'error'
                }) ;
            }
        ) ;
    },

    /**
     * Handler for when a destination file is selected. It copies the content to the ViewModel.
     * @param {Ext.form.field.File} fileField
     * @param {String} path
     * @param {Object} eOpts
     */
    onRightFileUpload: function (fileField, path, eOpts) {
        var me = this,
            viewModel = me.getViewModel() ;
        me._loadFile(fileField, 'rightFileXmlDocument', fileField).then(
            function(figXmlDocument) {
                var rightLayersStore = viewModel.get('rightLayersStore') ;
                rightLayersStore.getRoot().removeAll(true) ;
                SynfigTools.importCopyUtils.fillLayersStore(rightLayersStore, figXmlDocument) ;
                rightLayersStore.commitChanges() ;
            }
        ).otherwise(
            function(error) {
                SynfigTools.toastHelper.showErrorToast(error.message) ;
                Ext.log({
                    msg: error.stack,
                    level: 'error'
                }) ;
            }
        ) ;
    },

    /**
     * Handles the copy button click, copying the selected Xml Node (layer) at right, into the left root file.
     */
    onCopyButtonClick: function() {
        var me = this,
            viewModel = me.getViewModel(),
            leftXmlDoc = viewModel.get('leftFileXmlDocument'),
            leftXmlRootElement = leftXmlDoc.firstElementChild,
            rightXmlDoc = viewModel.get('rightFileXmlDocument'),
            importingLayerModel = viewModel.get('rightLayersTreePanel.selection'),
            leftLayersStore = viewModel.get('leftLayersStore');

        if (SynfigTools.importCopyUtils.xmlDocumentHasDefs(rightXmlDoc)) {
            SynfigTools.importCopyUtils.copyAllDefs(leftXmlDoc, rightXmlDoc) ;
            SynfigTools.toastHelper.showInfoToast(_('All ValueBase Nodes copied from source to target')) ;
        }

        if (SynfigTools.importCopyUtils.layerModelHasSkeleton(importingLayerModel)) {
            SynfigTools.importCopyUtils.copyAllBones(leftXmlDoc, rightXmlDoc) ;
            SynfigTools.toastHelper.showInfoToast(_('All bones nodes copied from source to target')) ;
        }

        SynfigTools.importCopyUtils.addLayerModelToLeft(importingLayerModel.clone(), leftLayersStore, leftXmlDoc) ;
        SynfigTools.toastHelper.showInfoToast(_('Layer copied')) ;
    },

    /**
     * Handles the merge button click. What merge does is copy everything from right layer that is missing in left layer.
     * Shows a window with merge options.
     */
    onMergeButtonClick: function() {
        var me = this ;

        var optionsWindow = Ext.create('SynfigTools.view.importcopy.MergeOptions', {
            viewModel: {
                parent: me.getViewModel()
            }
        }) ;
        optionsWindow.show() ;
    },

    onDownloadSifClick :function() {
        var me = this,
            viewModel = me.getViewModel(),
            serializer = new XMLSerializer(),
            xmlDocument = viewModel.get('leftFileXmlDocument'),
            xmlString = serializer.serializeToString(xmlDocument),
            fileContent = new Blob([xmlString]) ;
            fileName = Ext.String.createVarName(viewModel.get('leftFileTitle')) + '.sif' ;

        FileSaver.saveAs(fileContent, fileName) ;
    },

    onDownloadSifzClick :function() {
        var me = this,
            viewModel = me.getViewModel(),
            serializer = new XMLSerializer(),
            xmlDocument = viewModel.get('leftFileXmlDocument'),
            xmlString = serializer.serializeToString(xmlDocument),
            gzipFileContent = new Blob([new Uint8Array(gzip.zip(xmlString))], {type: 'application/octet-binary'}),
            fileName = Ext.String.createVarName(viewModel.get('leftFileTitle')) + '.sifz' ;

        FileSaver.saveAs(gzipFileContent, fileName) ;
    },

    /**
     * Loads the file selected at fileField. Sets the content of the file to the viewModelVar, and puts/removes a mask
     * on the component at maskComponent
     *
     * @param fileField
     * @param viewModelVar
     * @param maskComponent - Compontent to apply mask to
     * @private
     */
    _loadFile: function(fileField, viewModelVar, maskComponent) {
        var me = this,
            reader = new FileReader(),
            deferred = new Ext.Deferred()
            isGzipCompressedSifz = fileField.getValue().match(/\.sifz$/) !== null;

        maskComponent.mask() ;

        reader.onload = function (event) {
            var parser = new DOMParser(),
                fileContent = event.target.result ;

            if (isGzipCompressedSifz === true) {
                try {
                    fileContent = gzip.unzip(me._Uint8ToArray(new Uint8Array(fileContent)));
                } catch(error) {
                    SynfigTools.toastHelper.showErrorToast(error.message) ;
                    Ext.log({
                        msg: error.stack,
                        level: 'error'
                    }) ;
                }
            }
            var figXmlDocument = parser.parseFromString(fileContent, "application/xml"),
                rootXmlElement = figXmlDocument.querySelector('canvas') ;

            fileField.reset() ;
            if (rootXmlElement === null) {
                maskComponent.unmask() ;
                Ext.raise(_('<canvas> not found. Is it a .sif/.sifz file?')) ;
            }

            if (rootXmlElement.getAttribute('version') < 1) {
                Ext.MessageBox.alert('Old file version detected','Detected a file with version < 1.0. We recommend saving it in the new format with an updated version of Synfig') ;
            }

            me.getViewModel().set(viewModelVar, figXmlDocument) ;
            SynfigTools.toastHelper.showInfoToast(_('File loaded')) ;

            maskComponent.unmask() ;
            deferred.resolve(figXmlDocument) ;
        };

        reader.onerror = function () {
            SynfigTools.toastHelper.showErrorToast(_('Error loading file')) ;
            maskComponent.unmask() ;
            deferred.reject() ;
        };

        if (isGzipCompressedSifz) {
            reader.readAsArrayBuffer(fileField.fileInputEl.dom.files[0]) ;
        } else {
            reader.readAsText(fileField.fileInputEl.dom.files[0]) ;
        }

        return deferred.promise ;
    },

    /**
     * Creates a copy of a Uint8Array into a "normal" array.
     *
     * @param uint8Array
     * @private
     */
    _Uint8ToArray: function(uint8Array) {
        var resultArray = [] ;
        uint8Array.forEach(function (item) {
            resultArray.push(item) ;
        }) ;
        return resultArray ;
    },

    onCloseClick: function() {
        var me = this ;

        me.getView().close() ;
    }

}) ;