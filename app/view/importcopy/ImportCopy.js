Ext.define('SynfigTools.view.importcopy.ImportCopy', {
    extend: 'Ext.window.Window',

    requires: [
        'Ext.form.field.File',
        'SynfigTools.view.importcopy.ImportCopyController',
        'SynfigTools.view.importcopy.ImportCopyModel',
        'SynfigTools.view.importcopy.LayersTreePanel'
    ],

    controller: 'importcopy',
    viewModel: 'importcopy',
    scrollable: true,

    title: _('Copy layers'),

    bodyCls: 'window-body-padding',
    minWidth: 800, width: 800,
    minHeight: 400, height: 400,

    iconCls: 'icon-import',

    /**
     * Shows the WARNING, experimental, make backups! After the window is shown.
     * @override
     */
    afterShow: function() {
        Ext.MessageBox.alert(
            _('Make backups!'),
            _('This is an experimental unnofficial toolset, tested with Synfig 1.3.6, that copies layers and skeletons. Make backups of your data! Make backups!')
        ) ;
    },


    buttons: [
        {
            text: _('Close'),
            handler: 'onCloseClick'
        }
    ],

    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    items: [
        {
            xtype: 'layers-tree-panel',
            reference: 'leftLayersTreePanel',
            bind: {
                store: '{leftLayersStore}'
            },
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'splitbutton',
                            text: _('Download as .sifz'),
                            iconCls: 'x-fa fa-download',
                            handler: 'onDownloadSifzClick',
                            menu: [
                                {
                                    text: _('Download as .sif'),
                                    handler: 'onDownloadSifClick'
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    items: [
                        {
                            xtype: 'fileuploadfield',
                            buttonText: _('Load file'),
                            buttonOnly: true,
                            width: 100,
                            accept: '.sif,.sifz',
                            listeners: {
                                change: 'onLeftFileUpload'
                            }
                        },
                        {
                            xtype: 'textfield',
                            readOnly: true,
                            bind: {
                                value: '{leftFileTitle}'
                            }
                        }
                    ]
                }
            ],
            flex: 1
        },
        {
            xtype: 'container',
            width: 100,
            layout: {
                type: 'vbox',
                align: 'middle',
                pack: 'center'
            },
            defaults: {
                margin: 1,
            },
            items: [
                {
                    xtype: 'button',
                    text: _('copy'),
                    iconCls: 'x-fa fa-arrow-left',
                    bind: {
                        disabled: '{buttonCopyDisabled}'
                    },
                    handler: 'onCopyButtonClick'
                },
                // {
                //     xtype: 'button',
                //     text: _('merge'),
                //     iconCls: 'x-fa fa-arrow-left',
                //     bind: {
                //         disabled: '{buttonMergeDisabled}'
                //     },
                //     handler: 'onMergeButtonClick'
                // }
            ]
        },
        {
            xtype: 'layers-tree-panel',
            reference: 'rightLayersTreePanel',
            bind: {
                store: '{rightLayersStore}'
            },
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'top',
                    items: [
                        {
                            xtype: 'fileuploadfield',
                            buttonText: _('Load file'),
                            buttonOnly: true,
                            width: 100,
                            accept: '.sif,.sifz',
                            listeners: {
                                change: 'onRightFileUpload'
                            }
                        },
                        {
                            xtype: 'textfield',
                            readOnly: true,
                            bind: {
                                value: '{rightFileTitle}'
                            }
                        }
                    ]
                }
            ],
            flex: 1
        }
    ]

}) ;