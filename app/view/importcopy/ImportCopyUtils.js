Ext.define('SynfigTools.view.importcopy.ImportCopyUtils', {
    singleton: true,

    require: [
        'SynfigTools.model.Layer'
    ],

    /**
     * Given a Layer Store and a XmlDocument (.fig), takes all 'layer' children of the canvas and fills the Layers Store
     * @param {TreeStore} layersStore
     * @param {Document} figXmlDocument
     * @private
     */
    fillLayersStore: function(layersStore, figXmlDocument) {
        var me = this,
            rootXmlElement = figXmlDocument.querySelector('canvas') ;

        if (rootXmlElement === null) {
            Ext.raise(_('<canvas> not found. Is it a .sif/.sifz file?')) ;
        }
        me._fillLayerNodeWithXmlLayers(layersStore.getRoot(), rootXmlElement.childNodes) ;
    },

    /** Given a importing layer Model, it adds that element to the root of the given store, and updates the given XML
     * Document root with that layer.
     *
     * @param importedXmlNode
     * @param leftLayersStore
     * @param leftXmlDocument
     */
    addLayerModelToLeft: function(importingLayerModel, leftLayersStore, leftXmlDocument) {
        var me = this,
            leftXmlRootElement = leftXmlDocument.firstChild,
            importingLayerXml = importingLayerModel.get('xmlNode');

        leftLayersStore.getRoot().appendChild(importingLayerModel) ;
        leftXmlRootElement.appendChild(importingLayerXml) ;
    },

    /**
     * Checks recursively if a Layer Model has any descendant layer of type 'skeleton'
     * @param layerModel
     */
    layerModelHasSkeleton: function(layerModel) {
        var me = this ;

        return (layerModel.get('type') === 'skeleton') || (layerModel.findChild('type','skeleton',true) !== null) ;
    },

    /**
     * Checks if a document has ValueBase Nodes (<def> tags)
     * @param xmlDocument
     */
    xmlDocumentHasDefs: function(xmlDocument) {
        var me = this ;
        return xmlDocument.querySelector('canvas[version] > defs') !== null ;
    },

    /**
     * Copies all ValueBase Nodes from right document to left document, without overwriting existing values.
     * @param targetXmlDoc
     * @param sourceXmlDoc
     */
    copyAllDefs: function(targetXmlDoc, sourceXmlDoc) {
        var me = this,
            sourceDefs = sourceXmlDoc.querySelector('canvas[version] > defs'),
            targetDefs = targetXmlDoc.querySelector('canvas[version] > defs') ;

        // Copy all <defs> node to target if it does not exist
        if (targetDefs === null) {
            var firstLayer = targetXmlDoc.querySelector('canvas[version] > layer') ;
            if (firstLayer !== null) {
                targetXmlDoc.querySelector('canvas[version]').insertBefore(sourceDefs.cloneNode(true), firstLayer) ;
            } else {
                //There are no layers => appendChild
                targetXmlDoc.querySelector('canvas[version]').appendChild(sourceDefs.cloneNode(true)) ;
            }
            return ;
        }

        var sourceDefsList = sourceXmlDoc.querySelectorAll('canvas[version] > defs [id]') ;
        // For each element in source, check if the id exist at target and copy it if does not.
        sourceDefsList.forEach(function(sourceValueElement) {
            var id = sourceValueElement.getAttribute('id') ;
            if (targetDefs.querySelector(':scope [id="' + id + '"]') === null) {
                targetDefs.appendChild(sourceValueElement.cloneNode(true)) ;
            }
        }) ;

    },

    /**
     * Copy all non-existant bones from right XmlDoc to left XmlDoc.
     * @param targetXmlDoc
     * @param sourceXmlDoc
     */
    copyAllBones: function(targetXmlDoc, sourceXmlDoc) {
        var me = this,
            sourceBonesElement = sourceXmlDoc.querySelector('canvas[version] > bones') ;
            sourceAllBonesRootXmlNodeList = sourceXmlDoc.querySelectorAll('canvas[version] > bones > bone_root'),
            sourceAllBonesXmlNodeList = sourceXmlDoc.querySelectorAll('canvas[version] > bones > bone'),
            targetBonesElement = targetXmlDoc.querySelector('canvas[version] > bones') ;

        // Copy <bones> element at target if it doesn't exist
        if (targetBonesElement === null) {
            var firstLayer = targetXmlDoc.querySelector('canvas[version] > layer') ;
            if (firstLayer !== null) {
                targetXmlDoc.querySelector('canvas[version]').insertBefore(sourceBonesElement.cloneNode(true), firstLayer) ;
            } else {
                //There are no layers => appendChild
                targetXmlDoc.querySelector('canvas[version]').appendChild(sourceBonesElement.cloneNode(true)) ;
            }
            return ;
        }

        // Copy <bone_root> elements
        // Check that each bone_root does not exists at target before inserting it
        if (sourceAllBonesRootXmlNodeList !== null) {
            sourceAllBonesRootXmlNodeList.forEach(function(sourceBoneRootElement) {
                var boneRootGuid = sourceBoneRootElement.getAttribute('guid'),
                    targetBoneRoot = targetBonesElement.querySelector(':scope bone_root[guid="' + boneRootGuid +'"]') ;

                if (targetBoneRoot === null) {
                    // Does not exist => insert
                    targetBonesElement.appendChild(sourceBoneRootElement.cloneNode(true)) ;
                }
            }) ;
        }

        // Copy all <bone> elements
        if (sourceAllBonesXmlNodeList !== null) {
            sourceAllBonesXmlNodeList.forEach(function(sourceBoneElement) {
                var boneGuid = sourceBoneElement.getAttribute('guid'),
                    targetBone = targetBonesElement.querySelector(':scope bone[guid="' + boneGuid +'"]') ;

                if (targetBone === null) {
                    // Does not exist => insert
                    targetBonesElement.appendChild(sourceBoneElement.cloneNode(true)) ;
                }
            }) ;
        }

    },

    /**
     * Given a Layer Node and a XmlNode, takes all 'layer' children of XmlNode and inserts them into the LayerNode.
     * @param {SynfigTools.model.Layer} layerNode
     * @param {XmlNodeList} xmlNodesList
     * @private
     */
    _fillLayerNodeWithXmlLayers: function(layerNode, xmlNodesList) {
        var me = this ;

        xmlNodesList.forEach(
            function(xmlNode, index) {
                if (xmlNode.nodeName === 'layer') {
                    var createdChildLayerNode = me._addXmlNodeToStoreNode(layerNode, xmlNode),
                        xmlChildLayers = xmlNode.querySelectorAll(':scope > param[name=canvas] > canvas > layer') ;
                    me._fillLayerNodeWithXmlLayers(createdChildLayerNode, xmlChildLayers) ;
                }
            }
        ) ;
    },

    /**
     * Given a XmlNode and a Layer Node, it adds the XmlNode to the tree as a Layer model
     * @param xmlNode
     * @param layerNode
     * @return The created child Layer Node
     * @private
     */
    _addXmlNodeToStoreNode: function(layerNode, xmlNode) {
        var me = this,
            layerModel = Ext.create('SynfigTools.model.Layer') ;

        layerModel.loadFromXmlElement(xmlNode) ;
        return layerNode.insertChild(0, layerModel) ; // Layers are listed in .sif from bottom to top Z
        //return layerNode.appendChild(layerModel) ;
    },

    /**
     *
     * @param {Object} layersData
     * @param {SynfigTools.model.Layer} layersData.leftLayer
     * @param {SynfigTools.model.Layer} layersData.rightLayer
     *
     * @param {Object} mergeOptions
     * @param {boolean} [mergeOptions.copyAnimations=false],
     * @param {boolean} [mergeOptionsforceCopyAnimations=false]
     */
    mergeLayers: function(layersData, mergeOptions) {

    }


}, function(ImportCopyUtils) {
    SynfigTools.importCopyUtils = ImportCopyUtils ;
}) ;