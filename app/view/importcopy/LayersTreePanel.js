Ext.define('SynfigTools.view.importcopy.LayersTreePanel', {
    extend: 'Ext.tree.Panel',

    alias: 'widget.layers-tree-panel',

    minHeight: 100,
    rootVisible: false,
    useArrows: true,
    columns: [
        {
            xtype: 'treecolumn',
            text: _('Layer'),
            dataIndex: 'name',
            menuDisabled: true,
            sortable: false,
            flex: 2
        },
        {
            text: _('Type'),
            dataIndex: 'type',

            menuDisabled: true,
            sortable: false,
            width: 150
        },
        {
            xtype: 'checkcolumn',
            text: _('Is ref.'),
            dataIndex: 'isReference',
            menuDisabled: true,
            sortable: false,
            disabled: true,
            width: 60
        },
        {
            text: _('Ref. file'),
            dataIndex: 'referencedFile',
            menuDisabled: true,
            sortable:false,
            flex: 1
        }
    ]

}) ;