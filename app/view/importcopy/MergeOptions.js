Ext.define('SynfigTools.view.importcopy.MergeOptions', {
    extend: 'Ext.window.Window',

    requires: [
        'Ext.form.FieldContainer'
    ],

    title: _('Merge options'),

    viewModel: {
        data: {
            copyAnimations: false,
            forceCopyAnimations: false
        }
    },

    modal: true,

    iconCls: 'x-fa fa-gear',
    bodyCls: 'window-body-padding',

    width: 400,
    height: 400,
    minWidth: 400,
    minHeight: 400,

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    items: [
        {
            xtype: 'displayfield',
            value: '<p>This option will copy all animations found at left layer to the right layer when there is no animation:</p>'
        },
        {
            xtype: 'checkbox',
            boxLabel: _('Copy animations'),
            bind: {
                value: '{copyAnimations}'
            }
        },
        {
            xtype: 'displayfield',
            value: 'This option will force the copy, overwriting the existing animation:',
            bind: {
                disabled: '{!copyAnimations}'
            }
        },
        {
            xtype: 'checkbox',
            boxLabel: 'Force the copy of animations',
            bind: {
                disabled: '{!copyAnimations}',
                value: '{forceCopyAnimations}'
            }
        }
    ],

    buttons: [
        {
            text: _('Merge'),
            handler: function() {
                var viewModel = this.up('window').getViewModel() ;
                    layersData = {
                        leftLayer: viewModel.get('leftLayersTreePanel.selection'),
                        rightLayer: viewModel.get('rightLayersTreePanel.selection')
                    } ;

                SynfigTools.importCopyUtils.mergeLayers(layersData, viewModel.data /* options */) ;
            }
        },
        {
            text: _('Cancel'),
            handler: function() {
                this.up('window').close() ;
            }
        }
    ]

}) ;