/**
 * Module with the Import(copy) functionality
 */
Ext.define('SynfigTools.view.modules.ImportCopyModule', {
    extend: 'Ext.ux.desktop.Module',

    uses: [
        'SynfigTools.view.importcopy.ImportCopy'
    ],

    id: 'importcopymodule',

    /**
     * Launcher defines the menu item.
     */
    launcher: {
        text: _('Copy layers'),
        iconCls: 'icon-import',
        handler: null
    },

    /**
     * Factory method called then requested to create a window of a module.
     * Returns the window instante to show.
     */
    createWindow: function() {
        var me = this,
            desktopApp = me.app,
            desktop = desktopApp.getDesktop() ;

        return desktop.createWindow({}, SynfigTools.view.importcopy.ImportCopy) ;
    }

}) ;