/**
 * Module with the Import(copy) functionality
 */
Ext.define('SynfigTools.view.modules.ModsModule', {
    extend: 'Ext.ux.desktop.Module',

    uses: [
        'SynfigTools.view.mods.Mods'
    ],

    id: 'modsmodule',

    /**
     * Launcher defines the menu item.
     */
    launcher: {
        text: _('Mods'),
        iconCls: 'icon-puzzle',
        handler: null
    },

    /**
     * Factory method called then requested to create a window of a module.
     * Returns the window instante to show.
     */
    createWindow: function() {
        var me = this,
            desktopApp = me.app,
            desktop = desktopApp.getDesktop() ;

        return desktop.createWindow({}, SynfigTools.view.mods.Mods) ;
    }

}) ;