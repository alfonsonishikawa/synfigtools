/**
 * Window with the Settings
 */
Ext.define('SynfigTools.view.modules.TestsModule', {
    extend: 'Ext.ux.desktop.Module',

    id: 'testsmodule',

    uses: [
        'SynfigTools.view.tests.Tests'
    ],

    launcher: null, // No launcher in the start menu

    /**
     * Factory method called then requested to create a window of a module.
     * Returns the window instante to show.
     */
    createWindow: function() {
        var me = this,
            desktopApp = me.app,
            desktop = desktopApp.getDesktop() ;

        return desktop.createWindow({}, SynfigTools.view.tests.Tests) ;
    }

}) ;