/**
 * Window with the About information
 */
Ext.define('SynfigTools.view.modules.AboutModule', {
    extend: 'Ext.ux.desktop.Module',

    id: 'aboutmodule',

    /**
     * Launcher defines the menu item.
     */
    launcher: {
        text: _('About...'),
        iconCls: 'x-fa fa-info-circle',
        handler: null
    },

    /**
     * Factory method called then requested to create a window of a module.
     * Returns the window instante to show.
     */
    createWindow: function() {
        var me = this,
            desktopApp = me.app,
            desktop = desktopApp.getDesktop(),
            aboutWindow = desktop.getWindow('aboutwindow') ;

        if (!aboutWindow) {
            aboutWindow = desktop.createWindow(
                {
                    id: 'aboutwindow',
                    title: _('About SynfigTools'),
                    iconCls: 'x-fa fa-info-circle',
                    bodyCls: 'window-body-padding',
                    width: 600,
                    height: 400,
                    minWidth: 450,
                    minHeight: 300,
                    scrollable: true,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    dockedItems: [
                        {
                            xtype: 'toolbar',
                            dock: 'bottom',
                            items:[
                                {
                                    xtype: 'label',
                                    text: _('Version') + ': ' + Ext.manifest.version
                                },
                                '->',
                                {
                                    xtype: 'button',
                                    text: _('Close'),
                                    handler: function() {
                                        this.up('window').close() ;
                                    }
                                }
                            ]
                        }
                    ],
                    items: [
                        {
                            html: _('Synfig About 1'),
                            heigh: 110
                        },
                        {
                            xtype: 'textarea',
                            readOnly: true,
                            listeners: {
                                afterrender: function() {
                                    Ext.Ajax.request({
                                        url: 'LICENSE',
                                        success: function(response, opts) {
                                            this.setValue(response.responseText) ;
                                        },
                                        failure: function(response, opts) {
                                            this.setValue('Could not load the License file') ;
                                            Ext.log({level:'warn'}, 'Could not load License file') ;
                                        },
                                        scope: this
                                    })
                                }
                            },
                            flex: 1
                        }
                    ]
                }) ;
        }
        return aboutWindow ;
    }
}) ;