/**
 * Window with the Settings
 */
Ext.define('SynfigTools.view.modules.SettingsModule', {
    extend: 'Ext.ux.desktop.Module',

    id: 'settingsmodule',

    uses: 'SynfigTools.view.settings.Settings',

    launcher: null, // No launcher in the start menu

    /**
     * Factory method called then requested to create a window of a module.
     * Returns the window instante to show.
     */
    createWindow: function() {
        var me = this,
            desktopApp = me.app,
            desktop = desktopApp.getDesktop() ;

        return desktop.createWindow({}, SynfigTools.view.settings.Settings) ;
    }

}) ;