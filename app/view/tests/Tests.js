Ext.define('SynfigTools.view.tests.Tests', {
    extend: 'Ext.window.Window',

    title: _('Development tests module - Only for development'),

    requires: [
        'SynfigTools.view.importcopy.MergeOptions',
        'SynfigTools.view.tests.TestsController',
        'SynfigTools.view.tests.TestsModel'
    ],

    viewModel: 'tests',
    controller: 'tests',

    iconCls: 'x-fa fa-times-circle',
    bodyCls: 'window-body-padding',

    width: 600,
    height: 400,
    minWidth: 450,
    minHeight: 300,
    scrollable: true,

    layout: 'vbox',

    items: [
        {
            xtype: 'fieldcontainer',
            layout: 'hbox',
            fieldLabel: _('Merge layers'),
            items: [
                {
                    xtype: 'button',
                    text: 'Merge options',
                    handler: 'onMergeLayersTestClick'
                }
            ]
        }
    ],
    buttons: [
        {
            text: _('Close'),
            handler: function() {
                this.up('window').close() ;
            }
        }
    ],

}) ;