Ext.define('SynfigTools.view.tests.TestsController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.tests',

    uses: [
        'SynfigTools.view.importcopy.MergeOptions'
    ],

    onMergeLayersTestClick: function() {
        var me = this ;

        var optionsWindow = Ext.create('SynfigTools.view.importcopy.MergeOptions', {
            viewModel: {
                parent: me.getViewModel()
            }
        }) ;
        optionsWindow.show() ;
    }

}) ;