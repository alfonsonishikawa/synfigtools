Ext.define('SynfigTools.view.mods.ModsController', {
    extend: 'Ext.app.ViewController',

    alias:'controller.mods',

    uses: [
        'SynfigTools.helpers.toast.ToastHelper',
        'SynfigTools.view.importcopy.ImportCopyUtils'
    ],

    /**
     * Loads mods resources on the afterrender
     */
    afterRender() {
        var me = this ;
        me._loadCameraMod() ;
    },

    /**
     * Handler for when a source file is selected. It copies the content to the ViewModel.
     * @param {Ext.form.field.File} fileField
     * @param {String} path
     * @param {Object} eOpts
     */
    onFileUpload: function (fileField, path, eOpts) {
        var me = this,
            viewModel = me.getViewModel() ;

        me._loadFile(fileField, 'fileXmlDocument', fileField) ;
    },

    onDownloadSifClick :function() {
        var me = this,
            viewModel = me.getViewModel(),
            serializer = new XMLSerializer(),
            xmlDocument = viewModel.get('fileXmlDocument');

        me._applyMods() ;

        var xmlString = serializer.serializeToString(xmlDocument),
            fileContent = new Blob([xmlString]) ;
            fileName = Ext.String.createVarName(viewModel.get('fileTitle')) + '.sif' ;

        FileSaver.saveAs(fileContent, fileName) ;
    },

    onDownloadSifzClick :function() {
        var me = this,
            viewModel = me.getViewModel(),
            serializer = new XMLSerializer(),
            xmlDocument = viewModel.get('fileXmlDocument');

        me._applyMods() ;

        var xmlString = serializer.serializeToString(xmlDocument),
            gzipFileContent = new Blob([new Uint8Array(gzip.zip(xmlString))], {type: 'application/octet-binary'}),
            fileName = Ext.String.createVarName(viewModel.get('fileTitle')) + '.sifz' ;

        FileSaver.saveAs(gzipFileContent, fileName) ;
    },

    /**
     * Loads the file selected at fileField. Sets the content of the file to the viewModelVar, and puts/removes a mask
     * on the component at maskComponent
     *
     * @param fileField
     * @param viewModelVar
     * @param maskComponent - Compontent to apply mask to
     * @private
     */
    _loadFile: function(fileField, viewModelVar, maskComponent) {
        var me = this,
            reader = new FileReader(),
            deferred = new Ext.Deferred()
            isGzipCompressedSifz = fileField.getValue().match(/\.sifz$/) !== null;

        maskComponent.mask() ;

        reader.onload = function (event) {
            var parser = new DOMParser(),
                fileContent = event.target.result ;

            if (isGzipCompressedSifz === true) {
                try {
                    fileContent = gzip.unzip(me._Uint8ToArray(new Uint8Array(fileContent)));
                } catch(error) {
                    SynfigTools.toastHelper.showErrorToast(error.message) ;
                    Ext.log({
                        msg: error.stack,
                        level: 'error'
                    }) ;
                }
            }
            figXmlDocument = parser.parseFromString(fileContent, "application/xml"),
            rootXmlElement = figXmlDocument.querySelector('canvas') ;

            fileField.reset() ;
            if (rootXmlElement === null) {
                maskComponent.unmask() ;
                Ext.raise(_('<canvas> not found. Is it a .sif/.sifz file?')) ;
            }

            if (rootXmlElement.getAttribute('version') < 1) {
                Ext.MessageBox.alert('Old file version detected','Detected a file with version < 1.0. We recommend saving it in the new format with an updated version of Synfig') ;
            }

            me.getViewModel().set(viewModelVar, figXmlDocument) ;
            SynfigTools.toastHelper.showInfoToast(_('File loaded')) ;

            maskComponent.unmask() ;
            deferred.resolve(figXmlDocument) ;
        };

        reader.onerror = function () {
            SynfigTools.toastHelper.showErrorToast(_('Error loading file')) ;
            maskComponent.unmask() ;
            deferred.reject() ;
        };

        if (isGzipCompressedSifz) {
            reader.readAsArrayBuffer(fileField.fileInputEl.dom.files[0]) ;
        } else {
            reader.readAsText(fileField.fileInputEl.dom.files[0]) ;
        }

        return deferred.promise ;
    },

    _loadCameraMod: function() {
        var me = this ;

        var promise = Ext.Ajax.request({
            url: 'resources/mods/camera-view-03-1.3.6.sifz',
            binary: true
        }).then(function(response,opts){

            var bytes = response.responseBytes,
                parser = new DOMParser() ;

            try {
                fileContent = gzip.unzip(me._Uint8ToArray(bytes));
            } catch(error) {
                SynfigTools.toastHelper.showErrorToast(error.message) ;
                Ext.log({
                    msg: error.stack,
                    level: 'error'
                }) ;
            }

            var figXmlDocument = parser.parseFromString(fileContent, "application/xml") ;
            me.getViewModel().set('cameraModXmlDocument', figXmlDocument) ;

        }).otherwise(function(error){
            SynfigTools.toastHelper.showErrorToast(error.message) ;
            Ext.log({
                msg: error.stack,
                level: 'error'
            }) ;
        });

        return promise ;
    },

    _applyMods: function() {
        var me = this,
            viewModel = me.getViewModel(),
            applyCameraMod = viewModel.get('applyCameraMod') ;

        if (applyCameraMod) {
            me._applyCameraMod() ;
        }
    },

    _applyCameraMod: function() {
        var me = this,
            viewModel = me.getViewModel(),
            cameraXmlDoc = viewModel.get('cameraModXmlDocument'),
            targetXmlDoc = viewModel.get('fileXmlDocument'),
            targetXmlRootCanvas = targetXmlDoc.firstChild,
            viewBoxValues = Ext.String.splitWords(targetXmlRootCanvas.getAttribute('view-box')),
            cameraXmlLayers = cameraXmlDoc.querySelectorAll('canvas[version] > layer');

        // Copy all ValueBase Nodes
        SynfigTools.importCopyUtils.copyAllDefs(targetXmlDoc, cameraXmlDoc) ;

        // Copy all layers
        cameraXmlLayers.forEach(function(layerXmlElement) {
            targetXmlRootCanvas.appendChild(layerXmlElement.cloneNode(true)) ;
        })

        // Update height/width box of the camera. Bline Entries
        var vectorEntry = targetXmlDoc.querySelector('layer[type=group][desc=Camera] layer[type=outline] bline entry') ;
        vectorEntry.querySelector(':scope point x').textContent = viewBoxValues[0] ;
        vectorEntry.querySelector(':scope point y').textContent = viewBoxValues[1] ;

        vectorEntry = vectorEntry.nextElementSibling ;
        vectorEntry.querySelector(':scope point x').textContent = viewBoxValues[0] ;
        vectorEntry.querySelector(':scope point y').textContent = viewBoxValues[3] ;

        vectorEntry = vectorEntry.nextElementSibling ;
        vectorEntry.querySelector(':scope point x').textContent = viewBoxValues[2] ;
        vectorEntry.querySelector(':scope point y').textContent = viewBoxValues[3] ;

        vectorEntry = vectorEntry.nextElementSibling ;
        vectorEntry.querySelector(':scope point x').textContent = viewBoxValues[2] ;
        vectorEntry.querySelector(':scope point y').textContent = viewBoxValues[1] ;
    },

    /**
     * Creates a copy of a Uint8Array into a "normal" array.
     *
     * @param uint8Array
     * @private
     */
    _Uint8ToArray: function(uint8Array) {
        var resultArray = [] ;
        uint8Array.forEach(function (item) {
            resultArray.push(item) ;
        }) ;
        return resultArray ;
    },

    onCloseClick: function() {
        var me = this ;

        me.getView().close() ;
    }

}) ;