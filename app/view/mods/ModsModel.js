Ext.define('SynfigTools.view.mods.ModsModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.mods',

    requires: [
        'SynfigTools.model.Layer'
    ],

    data: {
        /**
         * Document with the XML tree
         */
        fileXmlDocument: null,

        /**
         * Document with the XML tree of the Camera Mod
         */
        cameraModXmlDocument: null,

        /**
         * Flag to know when to apply the camera mod
         */
        applyCameraMod: false,
    },

    formulas: {
        fileTitle: function(get) {
            var fileXmlDocument = get('fileXmlDocument') ;

            if (fileXmlDocument === null) return '' ;
            return fileXmlDocument.querySelector('canvas > name').textContent ;
        }

    }

}) ;