Ext.define('SynfigTools.view.mods.Mods', {
    extend: 'Ext.window.Window',

    requires: [
        'Ext.form.field.File',
        'Ext.form.FieldSet',
        'SynfigTools.view.mods.ModsController',
        'SynfigTools.view.mods.ModsModel'
    ],

    controller: 'mods',
    viewModel: 'mods',
    scrollable: true,

    title: _('Mods'),

    bodyCls: 'window-body-padding',
    minWidth: 800, width: 800,
    minHeight: 400, height: 400,

    iconCls: 'icon-puzzle',

    /**
     * Shows the WARNING, experimental, make backups! After the window is shown.
     * @override
     */
    afterShow: function() {
        Ext.MessageBox.alert(
            _('Make backups!'),
            _('This is an experimental unnofficial toolset, tested with Synfig 1.3.6, that copies layers and skeletons. Make backups of your data! Make backups!')
        ) ;
    },

    buttons: [
        {
            text: _('Close'),
            handler: 'onCloseClick'
        }
    ],

    layout: {
        type: 'vbox',
        align: 'stretch',
        pack: 'start'
    },
    items: [
        {
            xtype: 'container',
            layout: 'hbox',
            items: [
                {
                    xtype: 'fileuploadfield',
                    buttonText: _('Load file'),
                    buttonOnly: true,
                    width: 100,
                    accept: '.sif,.sifz',
                    listeners: {
                        change: 'onFileUpload'
                    }
                },
                {
                    xtype: 'displayfield',
                    fieldLabel: _('File title'),
                    bind: {
                        value: '{fileTitle}'
                    },
                    flex: 1
                }
            ]
        },
        {
            xtype: 'fieldset',
            title: _('Mods to install'),
            items: [
                {
                    xtype: 'checkbox',
                    fieldLabel: 'Camera view 0.3',
                    boxLabel: _('(Synfig 1.3.6)') + ' <a href="https://morevnaproject.org/2008/11/27/camera-widget/" target="_blank">https://morevnaproject.org/2008/11/27/camera-widget/</a>',
                    bind: {
                        value: '{applyCameraMod}'
                    }
                }
            ]
        },
        {
            xtype: 'container',
            layout: 'hbox',
            items: [
                {
                    xtype: 'splitbutton',
                    text: _('Download as .sifz'),
                    iconCls: 'x-fa fa-download',
                    handler: 'onDownloadSifzClick',
                    disabled: true,
                    bind: {
                        disabled: '{!fileXmlDocument}'
                    },
                    menu: [
                        {
                            text: _('Download as .sif'),
                            handler: 'onDownloadSifClick'
                        }
                    ]
                },
                {
                    xtype: 'box',
                    flex: 1
                }
            ]
        }
    ]

}) ;