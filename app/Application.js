/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('SynfigTools.Application', {
    extend: 'Ext.app.Application',

    name: 'SynfigTools',

    requires: [
        'SynfigTools.helpers.toast.ToastHelper',
        'Ext.layout.container.boxOverflow.Menu'
    ],

    quickTips: false,
    platformConfig: {
        desktop: {
            quickTips: true
        }
    },

    stores: [
        // TODO: add global / shared stores here
    ],

    launch: function () {
        // Hide the custom Loader Indicator
        LoaderIndicator.finishedLoading() ;
    },

    /**
     * A template method that is called when your application boots.
     * It is called before the Ext.app.Applications launch function is executed so gives a hook point to run
     * any code before your Viewport is created.
     *
     * Configures the state.Manager
     *
     * @param {Ext.app.Application} application
     */
    init: function (application) {
        var me = this ;

        Ext.state.Manager.setProvider(new Ext.state.LocalStorageProvider()) ;

        Ext.Error.handle = function(err) {
            SynfigTools.toastHelper.showGlobalErrorToast(err.msg) ;
        }

        // Load locale file
        var language = Ext.manifest.language,
            extJsLocaleFile = 'resources/i18n/locale/overrides/' + language + '/ext-locale-' + language +'.js' ;
        Ext.Loader.loadScript(extJsLocaleFile) ;

    },

    onAppUpdate: function () {
        Ext.Msg.show({
            title: _('Application Update'),
            icon: Ext.Msg.QUESTION,
            message: _('This application has an update, reload?'),
            buttons: Ext.Msg.YESNO,
            closable: false,
            callback: function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            },
            scope: this
        });
    }
});
